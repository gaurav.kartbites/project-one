package main

import (
    "bytes"
    "fmt"
    "log"
    "net/http"
    "os"
    "strconv"
    "database/sql"

    "github.com/gin-gonic/gin"
    "github.com/russross/blackfriday"
"github.com/Hepri/gin-jsonschema"

    _ "github.com/lib/pq"

)

var (
    repeat int
    db     *sql.DB
)

var testSchema string =`
{
  "todoitems": {
       "type": "object",
       "required": [
         "name",
         "status",
         "task"
       ],
       "properties": {
         "name": {
           "type": "string"
         },
         "task": {
           "type": "string"
         },
         "status": {
           "type": "string"
         }
       },
       //"example": "{\"name\":\"important task\",\"task\":\"preparing for exam\",\"status\":\"doing\"}"
     }
}`


func repeatFunc(c *gin.Context) {
    var buffer bytes.Buffer
    for i := 0; i < repeat; i++ {
        buffer.WriteString("Hello from Go!")
    }
    c.String(http.StatusOK, buffer.String())
}




type TODODATA struct {
	NAME   string `json:"name" binding:"required"`
	TASK   string `json:"task" binding:"required"`
	STATUS string `json:"status" binding:"required"`
}

func todoCreateFunc(c *gin.Context) {

	//name := c.PostForm("name")
	// task := c.PostForm("task")
	 //status := c.PostForm("status")
	 //message := " Creted name is " + name + " and task is " + task + " and status is " + status
	 //c.String(http.StatusOK, message)

   var tododata TODODATA
   	c.BindJSON(&tododata)

   if _, err := db.Exec("DROP TABLE todolist"); err != nil {
		c.String(http.StatusInternalServerError,
		fmt.Sprintf("Error creating database table: %q", err))
		return
	 }

	if _, err := db.Exec("CREATE TABLE IF NOT EXISTS todolist (id serial PRIMARY KEY, name text, task text, status text)"); err != nil {
			c.String(http.StatusInternalServerError,
	 				fmt.Sprintf("Error creating database table: %q", err))
	 		return
	 }

	 if _, err := db.Exec("INSERT INTO todolist (name, task, status) VALUES ('" + tododata.NAME + "','" + tododata.TASK + "', '"+ tododata.STATUS + "')"); err != nil {
	 		c.String(http.StatusInternalServerError,
	 				fmt.Sprintf("Error incrementing todolist: %q", err))
	 		return
	 }

	message := " Created name is  " + tododata.NAME + " and task is  " + tododata.TASK + "  and status is  " + tododata.STATUS
	c.String(http.StatusOK, message)
}

func todoReadFunc(c *gin.Context) {

	rows, err := db.Query("SELECT * FROM todolist")
	if err != nil {
			c.String(http.StatusInternalServerError,
					fmt.Sprintf("Error reading todolist: %q", err))
			return
	}

	defer rows.Close()
	for rows.Next() {
		var id int
			var name string
			var task string
			var status string
			if err := rows.Scan(&id,&name,&task,&status); err != nil {
				c.String(http.StatusInternalServerError,
					fmt.Sprintf("Error scanning todolist: %q", err))
					return
            }
            c.JSON(200, gin.H{
                "id":     id,
                "name":   name,
                "task":   task,
                "status": status,
            })

			//c.String(http.StatusOK, fmt.Sprintf("Read from DB  (name): %s\n", name))
			//c.String(http.StatusOK, fmt.Sprintf("Read from DB  (status): %s\n", status))
			//c.String(http.StatusOK, fmt.Sprintf("Read from DB  (task): %s\n", task))
	}



}





func main() {
    port := os.Getenv("PORT")

    if port == "" {
        log.Fatal("$PORT must be set")
    }

    var err error
    tStr := os.Getenv("REPEAT")
    repeat, err = strconv.Atoi(tStr)
    if err != nil {
        log.Print("Error converting $REPEAT to an int: %q - Using default", err)
        repeat = 5
    }

    db, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
    if err != nil {
        log.Fatalf("Error opening database: %q", err)
    }

    router := gin.New()
    router.Use(gin.Logger())
    router.LoadHTMLGlob("templates/*.tmpl.html")
    router.Static("/static", "static")
	//	router.POST("/todoitem", todoCreateFunc)
router.POST("/todoitem", schema.Validate(todoCreateFunc, testSchema))


		router.GET("/todoitems", todoReadFunc)
    router.GET("/", func(c *gin.Context) {
        c.HTML(http.StatusOK, "index.tmpl.html", nil)
    })

    router.GET("/mark", func(c *gin.Context) {
        c.String(http.StatusOK, string(blackfriday.MarkdownBasic([]byte("**hi!**"))))
    })

    router.GET("/repeat", repeatFunc)


    router.Run(":" + port)
}
